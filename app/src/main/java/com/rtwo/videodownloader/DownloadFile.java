package com.rtwo.videodownloader;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

/**
 * Created by Administrator on 2015/11/25.
 */

//文件下载
public class DownloadFile {

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Context context;
    private String name;
    private String url;

    public DownloadFile(Context context,String url,String name){
        this.context = context;
        this.url = url;
        this.name = name;
    }

    void goDownload() {
        downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
        request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalPublicDir("/videodownloader/video",name);
        request.setTitle("文件下载");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        downloadManager.enqueue(request);
    }
}
