package com.rtwo.videodownloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

/**
 * Created by ThinkPad on 2015/11/26.
 */
public class DownloadCompletedReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"您的一个视频完成了下载",Toast.LENGTH_SHORT).show();
        MediaPlayer.create(context,R.raw.download_complete).start();
    }
}
