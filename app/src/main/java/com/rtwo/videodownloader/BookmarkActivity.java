package com.rtwo.videodownloader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.Arrays;
import java.util.HashMap;


/**
 * Created by ThinkPad on 2015/11/30.
 */
public class BookmarkActivity extends Activity{
    private SwipeMenuListView bookmark;
    private ArrayAdapter<String> bookmarkAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        bookmark = (SwipeMenuListView)findViewById(R.id.bookmark_listview);
        bookmark.setMenuCreator(creator);
        bookmarkAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, getBookmarksTitle());
        bookmark.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                SharedPreferences sb = BookmarkActivity.this.getSharedPreferences("bookmark_title", MODE_PRIVATE);
                SharedPreferences sb1 = BookmarkActivity.this.getSharedPreferences("bookmark", MODE_PRIVATE);
                switch (index) {
                    case 0:
                        String address = sb1.getString("bookmark", "").split("&")[position];
                        Intent intent = getIntent();
                        intent.setData(Uri.parse(address));
                        setResult(RESULT_OK, intent);
                        finish();
                        break;
                    case 1:
                        sb.edit().putString("bookmark_title", sb.getString("bookmark_title", "").replace(sb.getString("bookmark_title", "").split("&")[position] + "&", "")).commit();
                        sb1.edit().putString("bookmark", sb.getString("bookmark", "").replace(sb.getString("bookmark", "").split("&")[position] + "&", "")).commit();
                        bookmarkAdapter.remove(bookmarkAdapter.getItem(position));
                        bookmarkAdapter.notifyDataSetChanged();
                        // delete
                        break;
                    default:
                        break;
                }
                // false : close the menu; true : not close the menu
                return true;
            }
        });
        bookmark.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        bookmark.setAdapter(bookmarkAdapter);
        bookmark.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences sb = BookmarkActivity.this.getSharedPreferences("bookmark", MODE_PRIVATE);
                String address = sb.getString("bookmark", "").split("&")[position];
                Intent intent = getIntent();
                intent.setData(Uri.parse(address));
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }

    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            // create "open" item
            SwipeMenuItem openItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background
//            openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
//                    0xCE)));
            // set item width
            openItem.setWidth(dp2px(90));
            // set item title
//            openItem.setTitle("Open");
            // set item title fontsize
//            openItem.setTitleSize(18);
            // set item title font color
//            openItem.setTitleColor(Color.WHITE);
            openItem.setIcon(R.drawable.ic_open);
            // add to menu
            menu.addMenuItem(openItem);

            // create "delete" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background
//            deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
//                    0x3F, 0x25)));
            // set item width
            deleteItem.setWidth(dp2px(90));
            // set a icon
            deleteItem.setIcon(R.drawable.ic_delete);
            // add to menu
            menu.addMenuItem(deleteItem);
        }
    };

    String[] getBookmarksTitle(){
        SharedPreferences sb = this.getSharedPreferences("bookmark_title",MODE_PRIVATE);
        return sb.getString("bookmark_title","").split("&");
    }

    String[] getBookmarks(){
        SharedPreferences sb = this.getSharedPreferences("bookmark",MODE_PRIVATE);
        return sb.getString("bookmark","").split("&");
    }

    HashMap<String,String> getAddressForTitle(){
        HashMap<String,String> map = new HashMap<String,String>();
        for(int i=0;i<getBookmarks().length;i++)
            map.put(getBookmarksTitle()[i],getBookmarks()[i]);
        return map;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
