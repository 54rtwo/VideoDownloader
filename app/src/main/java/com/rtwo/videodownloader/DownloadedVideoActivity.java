package com.rtwo.videodownloader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

/**
 * Created by Administrator on 2015/11/30.
 */
public class DownloadedVideoActivity extends Activity {
    private SwipeMenuListView downloadedVideo;
    private ArrayAdapter<String> downloadedVideoAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        downloadedVideo = (SwipeMenuListView)findViewById(R.id.download_video);
        downloadedVideo.setMenuCreator(creator);
        downloadedVideoAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,getDownloadedVideo());
        downloadedVideo.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                String name = downloadedVideoAdapter.getItem(position).split("  size:")[0];
                switch (index) {
                    case 0:
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse(Environment.getExternalStorageDirectory()+"/videodownloader/video/"+name), "video/mp4");
                        startActivity(intent);
                        break;
                    case 1:
                        File file = new File(Environment.getExternalStorageDirectory()+"/videodownloader/video/"+name);
                        file.delete();
                        downloadedVideoAdapter.remove(downloadedVideoAdapter.getItem(position));
                        downloadedVideoAdapter.notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        downloadedVideo.setAdapter(downloadedVideoAdapter);
        downloadedVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = downloadedVideoAdapter.getItem(position).split("  size:")[0];
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(Environment.getExternalStorageDirectory()+"/videodownloader/video/"+name), "video/mp4");
                startActivity(intent);
            }
        });
    }

    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem openItem = new SwipeMenuItem(
                    getApplicationContext());
            openItem.setWidth(dp2px(90));
            openItem.setIcon(R.drawable.ic_open);
            menu.addMenuItem(openItem);
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    getApplicationContext());
            deleteItem.setWidth(dp2px(90));
            deleteItem.setIcon(R.drawable.ic_delete);
            menu.addMenuItem(deleteItem);
        }
    };

    ArrayList<String> getDownloadedVideo(){
        File file = new File(Environment.getExternalStorageDirectory()+"/videodownloader/video");
        String[] files = file.list(new FilePicker());
        ArrayList<String> fileInfo = new ArrayList<String>();
        for(String i:files){
            float size = new File(Environment.getExternalStorageDirectory()+"/videodownloader/video/"+i).length()/1024;
            String sizeStr = null;
            if(size>=1024){
                sizeStr = size/1024+"MB";
            }
            else{
                sizeStr = size+"KB";
            }
            fileInfo.add(i+"  size:"+sizeStr);
        }
        return fileInfo;
    }

    class FilePicker implements FilenameFilter{
        @Override
        public boolean accept(java.io.File dir, String filename) {
            return filename.endsWith("mp4");
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
