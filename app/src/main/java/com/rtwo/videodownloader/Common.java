package com.rtwo.videodownloader;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2015/11/25.
 */

//通用类
public class Common {

    private View object;
    private Context context;

    //如果需要调用动画方法,使用这个构造函数
    public Common(View object){
        this.object = object;
    }

    //如果需要调用AutoCompleteTextView控件的相关方法,使用这个构造函数
    public Common(Context context){
        this.context = context;
    }

    //尺寸变化动画方法
    public void scaleAnimate(){
        ScaleAnimation animation = new ScaleAnimation(0.1f,1.0f,0.1f,1.0f, RotateAnimation.RELATIVE_TO_SELF,0.5f,RotateAnimation.RELATIVE_TO_SELF,0.5f);
        animation.setDuration(500);
        object.startAnimation(animation);
    }

    //保存AutoCompleteTextView控件的历史数据到指定的字段
    public void storeHistoryData(AutoCompleteTextView textView,String field){
        String content = textView.getText().toString();
        SharedPreferences sp = context.getSharedPreferences("history_url",Context.MODE_PRIVATE);
        String history = sp.getString(field, "");
        if(!history.contains(content)){
            StringBuilder sb = new StringBuilder(history);
            sb.insert(0,content+"&");
            sp.edit().putString(field,sb.toString()).apply();
        }
    }

    //设置AutoCompleteTextView
    public void initAutoComplete(String field,AutoCompleteTextView auto) {
        String history = context.getSharedPreferences("history_url", Context.MODE_PRIVATE).getString(field, "");
        String[] hisArrays = history.split("&");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, hisArrays);
        //只保留最近的10条的记录
        if(hisArrays.length > 10){
            String[] newArrays = new String[10];
            System.arraycopy(hisArrays, 0, newArrays, 0, 10);
            adapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_list_item_1, newArrays);
        }
        auto.setAdapter(adapter);
        auto.setDropDownHeight(500);
        auto.setThreshold(1);
        auto.setCompletionHint("最近的10条记录");
        auto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                AutoCompleteTextView view = (AutoCompleteTextView) v;
                if (hasFocus) {
                    view.showDropDown();
                }
            }
        });
    }

    //保存指定内容到指定的字段在sharedpreference中
    public boolean storeData(String folderName,String field,String data){
        SharedPreferences sp = context.getSharedPreferences(folderName, Context.MODE_PRIVATE);
        String history = sp.getString(field, "");
        if(!history.contains(data)){
            StringBuilder sb = new StringBuilder(history);
            sb.insert(0,data+"&");
            sp.edit().putString(field,sb.toString()).apply();
        }
        return sp.getString(field,"").contains(data);
    }

    public void saveBitmap(Bitmap bm,String picName) {
        File f = new File(Environment.getExternalStorageDirectory()+"/videodownloader/pics/", picName);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}

